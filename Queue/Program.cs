﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            
        }
    }
    class Calculate
    {

    }
    class Producer
    {

    }
    class Consumer
    {
        
      
    }
    public class SingletonConcurrentQueue<T> : ConcurrentQueue<T>
    {
        private static readonly SingletonConcurrentQueue<T>
                                       _instance = new SingletonConcurrentQueue<T>();

        static SingletonConcurrentQueue() { }
        private SingletonConcurrentQueue() { }

        public static SingletonConcurrentQueue<T> Instance
        {
            get { return _instance; }
        }
    }
}

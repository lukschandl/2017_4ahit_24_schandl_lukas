﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePattern
{
    public abstract class Mitarbeiter
    {
        public abstract int getMitarbeiterAnzahl();

        public abstract void print(string abstand);

        private string name;
        private int telefonNr;

        public Mitarbeiter(string name, int telefonNr)
        {
            this.name = name;
            this.telefonNr = telefonNr;
        }

        public string getName()
        {
            return name;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public int getTelefonNr()
        {
            return telefonNr;
        }

        public void setTelefonNr(int telefonNr)
        {
            this.telefonNr = telefonNr;
        }
    }
}

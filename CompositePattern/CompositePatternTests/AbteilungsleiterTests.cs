﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CompositePattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompositePattern.Tests
{
    [TestClass()]
    public class AbteilungsleiterTests
    {
        Abteilungsleiter al1 = new Abteilungsleiter("W. Fischer", "Vertrieb", 001);

        [TestMethod()]
        public void getMitarbeiterAnzahlTest()
        {
            al1.add(new AtomarerMitarbeiter("P. Meier", 123));
            al1.add(new AtomarerMitarbeiter("I. Schulz", 112));

            Assert.AreEqual(al1.getMitarbeiterAnzahl(), 3);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace _01_Projekt_MovingBall
{

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        List<IShape> shapelist = new List<IShape>();
        public Color myColor;
        List<AShape> shapes = new List<AShape>();
        public DispatcherTimer mainEllipses = new DispatcherTimer();
        public AnalyseColor analyseColor = new AnalyseColor();
        public MainWindow()
        {
            InitializeComponent();
            RenderOptions.SetBitmapScalingMode(Fundament, VisualBitmapScalingMode);
            myColor = Color.FromRgb(255, 0, 0);
            FundamentCanvas.Fundament = Fundament;
        }
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            byte[] rgb = new byte[3];
            rgb = analyseColor.colorAnalyse(checkboxrot.IsChecked, checkboxgrün.IsChecked, checkboxblau.IsChecked);
            myColor = Color.FromRgb(rgb[0], rgb[1], rgb[2]); 
        }
        private void circle_Click(object sender, RoutedEventArgs e)
        {
            IShape ellipse = new EllipseShape( myColor);
            ellipse.init(20,20);
            ellipse.Start();
            shapelist.Add(ellipse);
        }
        private void Rectangle_Click_1(object sender, RoutedEventArgs e)
        {
            IShape rectangle = new RectangleShape( myColor);
            rectangle.init(20, 20);
            rectangle.Start();
            shapelist.Add(rectangle);
        }

        private void Triangle_Click(object sender, RoutedEventArgs e)
        {
            IShape triangle = new TriangleShape(myColor);
            triangle.init(20, 20);
            triangle.Start();
            shapelist.Add(triangle);
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (shapelist.Count != 0)
            {
                int index = shapelist.Count - 1;
                IShape d = shapelist[index];
                d.Stop();
                Fundament.Children.Remove(d.getShape());
                shapelist.RemoveAt(shapelist.Count - 1);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _01_Projekt_MovingBall
{
    public abstract class Physics
    {
        double x;
        bool goingRight;
        double y;
        bool goingTop;
        double speed = 10;
        protected void Move(Shape shape)
        {
            
            x = Canvas.GetLeft(shape);
            y = Canvas.GetTop(shape);

            if (goingRight)
            {
                x +=speed;
                if(x+ shape.Width > FundamentCanvas.Fundament.ActualWidth)
                {
                    x = FundamentCanvas.Fundament.ActualWidth - shape.Width;
                    goingRight = false;
                }
            }
            else if(!goingRight)
            {
                x -= speed;
                if (x < 0)
                {
                    x = 0;
                    goingRight = true;
                }
            }

            if (goingTop)
            {
                y += speed;
                if (y+ shape.Height > FundamentCanvas.Fundament.ActualHeight)
                {
                    y = FundamentCanvas.Fundament.ActualHeight- shape.Height;
                    goingTop = false;
                }
            }
            else if(!goingTop)
            {
                y -= speed;
                if (y < 70)
                {
                    y = 70;
                    goingTop = true;
                }
            }
            Canvas.SetTop(shape, y);
            Canvas.SetLeft(shape, x);
        }
    }
}

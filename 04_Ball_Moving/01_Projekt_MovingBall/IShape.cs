﻿using System.Windows.Input;
using System.Windows.Shapes;

namespace _01_Projekt_MovingBall
{
    public interface IShape
    {
        Shape getShape();
        void init(int heigt, int width);
        void MouseDown(object sender, MouseButtonEventArgs e);
        void Start();
        void Stop();
    }
}
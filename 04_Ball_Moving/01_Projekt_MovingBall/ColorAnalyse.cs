﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Projekt_MovingBall
{

    public class AnalyseColor
    {
        public byte[] colorAnalyse(bool? red, bool? green, bool? blue)
        {
            if (red == true && green == false && blue == false)
            {
                return new byte[3] { 255, 0, 0 };
            }
            if(red == false && green == true && blue == false)
            {
                return new byte[3] { 0, 255, 0 };
            }
            if(red == false && green == false && blue == true)
            {
                return new byte[3] { 0, 0, 255 };
            }

            if (red == true && green == true && blue == false)
            {
                return new byte[3] { 255, 255, 0 };
            }
            if (red == true && green == false && blue == true)
            {
                return new byte[3] { 255, 0, 255 };
            }

            if (red == false && green == true && blue == true)
            {
                return new byte[3] { 0, 255, 255 };
            }
            if (red == true && green == true && blue == true)
            {
                return new byte[3] { 255, 255, 255 };
            }
            if (red == false && green == false && blue == false)
            {
                return new byte[3] { 1, 1, 1 };
            }
            return null;
        }
    }
}

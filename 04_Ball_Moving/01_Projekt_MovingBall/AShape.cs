﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace _01_Projekt_MovingBall
{
    
    public abstract class AShape : Physics, IShape
    {
      
        protected SolidColorBrush brushes = new SolidColorBrush();
        protected DispatcherTimer dispatcherTimer = new DispatcherTimer();
        public int counter = 0;
        public abstract Shape getShape();
        public abstract void init(int heigt, int width);
        public void MouseDown(object sender, MouseButtonEventArgs e)
        {
            counter++;
        }
        protected void timer()
        {

            RenderShape.MouseDown += new MouseButtonEventHandler(MouseDown);
            dispatcherTimer.Tick += Moving;
            dispatcherTimer.Interval = TimeSpan.FromSeconds(0.05);

        }
        static Random randy = new Random();
        protected void position()
        {
            Canvas.SetLeft(RenderShape, randy.Next(0,Convert.ToInt32(FundamentCanvas.Fundament.ActualWidth)));
            Canvas.SetTop(RenderShape, randy.Next(0,Convert.ToInt32(FundamentCanvas.Fundament.ActualHeight)));
            FundamentCanvas.Fundament.Children.Add(RenderShape);
        }
        protected abstract void Moving(object sender, EventArgs e);
        protected Shape RenderShape = null;
        public void Start()
        {
            dispatcherTimer.Start();
        }
        public void Stop()
        {
            dispatcherTimer.Stop();
        }
    }
    class EllipseShape : AShape
    {
       
        public EllipseShape()
        {
            
            brushes.Color = Color.FromRgb(255, 0, 255);
        }
        public EllipseShape(Color myColor)
        {
        
            
            brushes.Color = myColor;
        }
        public override Shape getShape()
        {
            return RenderShape;
        }
        public override void init(int height, int width)
        {
            
            RenderShape = new Ellipse() { Height = height, Width = width };
            RenderShape.Fill = brushes;
            timer();
            position();
        }
        protected override void Moving(object sender, EventArgs e)
        {
            Move(RenderShape);
        }
    }
    class RectangleShape : AShape
    {
        public RectangleShape()
        {
            brushes.Color = Color.FromRgb(255, 0, 0);
          
        }
        public RectangleShape( Color myColor)
        {
            
            brushes.Color = myColor;
        }
        public override Shape getShape()
        {
            return RenderShape;
        }

        public override void init(int height, int width)
        {
            
            RenderShape = new Rectangle() { Height = height, Width = width };
            RenderShape.Fill = brushes;
            timer();
            position();
        }

        protected override void Moving(object sender, EventArgs e)
        {
            Move(RenderShape);
        }
    }
    class TriangleShape : AShape
    {
        public TriangleShape()
        {
            
            brushes.Color = Color.FromRgb(0, 255, 0);
        }
        public TriangleShape( Color myColor)
        {
           
            brushes.Color = myColor;
        }
        public override Shape getShape()
        {
            return RenderShape;
        }

        public override void init(int height, int width)
        {
            PointCollection myPointCollection = new PointCollection();
            myPointCollection.Add(new Point(20, 1));
            myPointCollection.Add(new Point(22, 25));
            myPointCollection.Add(new Point(45, 10));
            RenderShape = new Polygon() { Points = myPointCollection};
            RenderShape.Height = 20;
            RenderShape.Width = 40;
            RenderShape.Fill = brushes;        
            timer();
            position();
        }

        protected override void Moving(object sender, EventArgs e)
        {
            Move(RenderShape);
        }
    }
}

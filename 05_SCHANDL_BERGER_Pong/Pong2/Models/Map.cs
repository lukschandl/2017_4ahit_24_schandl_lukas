﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Pong2.Models
{
    class Map
    {
        public Square PlayerOne { get; set; }
        public Square PlayerTwo { get; set; }
        public Ball ball { get; set; }
        public Players players { get; set; }
        public DispatcherTimer dispatcherTimer { get; set; }
        public Canvas GameCanvas { get; set; }
        public bool IsActive { get; set; }
        public int MaxPoints { get; set; }

        public MainWindow mainWindow { get; set; }


        public Map(MainWindow mainWindow)
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            dispatcherTimer.IsEnabled = false;
            dispatcherTimer.Tick += new EventHandler(Dpt_tick);


            GameCanvas = mainWindow.myCanvas;
            ball = new Ball(GameCanvas);
            PlayerOne = new Square(15);
            PlayerTwo = new Square(430);
            players = new Players();
            this.mainWindow = mainWindow;

            MaxPoints = 11; //in Mainviewmodel?

            GameCanvas.Children.Add(PlayerOne.rectangle);
            GameCanvas.Children.Add(PlayerTwo.rectangle);
            GameCanvas.Children.Add(ball.circle);
        }

        public void PlayPause()
        {
            if (dispatcherTimer.IsEnabled)
            {
                dispatcherTimer.IsEnabled = false;
                mainWindow.playpause.Content = "Play"; //?
            }
            else
            {
                dispatcherTimer.IsEnabled = true;
                mainWindow.playpause.Content = "Pause"; //?
            }
        }

        public void Dpt_tick(object sender, EventArgs e)
        {
            if (ball.X <= 4)
            {
                ball.moveXDirection();
                players.PointsPlayerTwo++;

                if (players.PointsPlayerTwo == MaxPoints)
                {
                    MessageBox.Show("Player 2 Won!");
                    players.Reset(mainWindow);
                    ball.Reset("mid");
                    PlayPause();

                }
                else
                    Reset("left");

                mainWindow.lblPlayerTwoPoints.Content = players.PointsPlayerTwo;
            }
            if (ball.X >= 435)
            {
                ball.moveXDirection();
                players.PointsPlayerOne++;

                if (players.PointsPlayerOne == MaxPoints)
                {
                    MessageBox.Show("Player 1 Won!");
                    players.Reset(mainWindow);
                    ball.Reset("mid");
                    PlayPause();
                }
                else
                    Reset("right");

                mainWindow.lblPlayerOnePoints.Content = players.PointsPlayerOne;
            }

            if (ball.Y <= 0 || ball.Y >= 234)
            {
                ball.moveYDirection();
            }

            if (ball.X <= (PlayerOne.X + PlayerOne.Width) && ball.Y >= PlayerOne.Y
                && ball.Y <= (PlayerOne.Y + PlayerOne.Height))
            {
                ball.moveXDirection();
            }
            if ((ball.X + ball.Size) >= PlayerTwo.X && ball.Y >= PlayerTwo.Y
                && ball.Y <= (PlayerTwo.Y + PlayerTwo.Height))
            {
                ball.moveXDirection();
            }

            ball.Movement();
            PlayerOne.Movement();
            PlayerTwo.Movement();
        }



        public void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && PlayerOne.Y < 184)
            {
                PlayerOne.Down();
            }
            if (e.Key == Key.W && PlayerOne.Y > 0)
            {
                PlayerOne.Up();
            }
            if (e.Key == Key.Up && PlayerTwo.Y > 0) //prev playerone
            {
                PlayerTwo.Up();
            }
            if (e.Key == Key.Down && PlayerTwo.Y < 184) //prev playerone
            {
                PlayerTwo.Down();
            }

        }

        public void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.W || e.Key == Key.S)
            {
                PlayerOne.stop();
            }
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                PlayerTwo.stop();
            }
        }


        private void Reset(string ballLocation)
        {
            PlayPause();
            ball.Reset(ballLocation);
            PlayerOne.Reset();
            PlayerTwo.Reset();
        }

    }
}

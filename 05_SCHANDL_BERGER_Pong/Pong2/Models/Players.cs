﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong2
{
    //ViewModel
    class Players
    {
        public int PointsPlayerOne { get; set; }  
        public int PointsPlayerTwo { get; set; }
        

        public Players()
        {
            PointsPlayerOne = 0;
            PointsPlayerTwo = 0;
        }

        public void Reset(MainWindow mainWindow)
        {
            PointsPlayerOne = 0;
            PointsPlayerTwo = 0;

            mainWindow.lblPlayerOnePoints.Content = 0;
            mainWindow.lblPlayerTwoPoints.Content = 0;
        }
    }
}

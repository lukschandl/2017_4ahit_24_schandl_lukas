﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Controls;

namespace Pong2
{
    class Square
    {
        public Rectangle rectangle;
        int yDirection = 0;
        public int X, Y;
        public int Width, Height;
        int mov = 3;
        bool goesIn = false;

        public Square PlayerOne { get; set; }
        public Square PlayerTwo { get; set; }

        //hier mycanvas rausgeben, rectangle noch nirgendes reingeben 
        public Square(int x)
        {
            rectangle = new Rectangle();

            SolidColorBrush squareColor = new SolidColorBrush();
            squareColor.Color = Color.FromRgb(0, 0, 255);
            rectangle.Fill = squareColor;

            Width = 25;
            Height = 75;
            rectangle.Width = Width;
            rectangle.Height = Height;

            this.X = x;
            Y = 80;

            Canvas.SetTop(rectangle, Y);
            Canvas.SetLeft(rectangle, this.X);

        }
        
        //map klasse
        //3 Ordner Viewmodels, Model, View

        public void Reset()
        {
            Y = 80;
        }


        public void Movement()
        {
            Y += yDirection;
            Canvas.SetTop(rectangle, Y);

            if (Y >= 186 && !goesIn)
            {
                yDirection = 0;
                
                goesIn = true;
            }
            else if (Y <= 0 && !goesIn)
            {
                yDirection = 0;
                
                goesIn = true;
            }
            else
                goesIn = false;

                
        }


        public void Down()
        {
            if (Y >= 184)
            {
                yDirection = 0;
            }
            else
            {
                yDirection = mov;
            }
        }
        
        public void Up()
        {

            if (Y <= 0)
            {
                yDirection = 0;
            }
            else
            {
                yDirection = -(mov);
            }
        }
        
        public void stop()
        {
            yDirection = 0;
        }
    }
}

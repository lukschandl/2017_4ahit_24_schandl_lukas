﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Controls;
using Pong2.Models; //diesen hier einbinden

namespace Pong2
{
    //Viewmodel
    class Ball
    {

        public Ellipse circle { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Size { get; set; }
        int xDirection, yDirection;
        int mov = 2;


        public Ball(Canvas c)
        {
            circle = new Ellipse();

            SolidColorBrush color = new SolidColorBrush();
            color.Color = Color.FromRgb(255, 0, 0);
            circle.Fill = color;

            Reset("mid");
            Size = 20;
            xDirection = mov;
            yDirection = mov;
            circle.Width = Size;
            circle.Height = Size;

            Canvas.SetTop(circle, Y);
            Canvas.SetLeft(circle, X);
        }

        public void Reset(string location)
        {

            if (location == "left")
            {
                X = 100;
                Y = 100;
            }
            else if (location == "right")
            {
                X = 350;
                Y = 100;
            }
            else if (location == "mid")
            {
                X = 220;
                Y = 110;
            }

        }

        public void Movement()
        {
            X += xDirection;
            Y += yDirection;
            Canvas.SetTop(circle, Y);
            Canvas.SetLeft(circle, X);

        }

        public void moveXDirection()
        {
            if (xDirection == mov)
            {
                xDirection = -(mov);
            }
            else
            {
                xDirection = mov;
            }
        }

        public void moveYDirection()
        {
            if (yDirection == mov)
            {
                yDirection = -(mov);
            }
            else
            {
                yDirection = mov;
            }
        }
    }

}

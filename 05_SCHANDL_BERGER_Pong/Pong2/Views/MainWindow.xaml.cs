﻿using Pong2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Pong2
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Players players = new Players();

        MainViewModel mainViewModel;

        public MainWindow()
        {
            InitializeComponent();
            mainViewModel = new MainViewModel(this);

            DataContext = mainViewModel;
        }


        private void PlayPause_Click(object sender, RoutedEventArgs e)
        {
            mainViewModel.GameMap.PlayPause();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            mainViewModel.GameMap.Window_KeyDown(sender, e);
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            mainViewModel.GameMap.Window_KeyUp(sender, e);
        }


    }
}

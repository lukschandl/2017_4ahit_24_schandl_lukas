﻿using Pong2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong2.ViewModels
{
    class MainViewModel
    {
        public Map GameMap { get; set; }
        

        public MainViewModel(MainWindow mainWindow)
        {
            GameMap = new Map(mainWindow);

        }
    }
}

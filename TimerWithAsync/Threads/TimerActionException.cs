﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Threads
{
    class TimerActionException : Exception
    {
        public TimerActionException(string message) : base(message) { }
    }
}

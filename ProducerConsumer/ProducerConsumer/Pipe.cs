﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProducerConsumer
{
    class Pipe : Queue<Calculate>
    {
        private static Pipe pipe;

        private Pipe()
        {}

        public static Pipe getPipe()
        {
            if (pipe == null)
                pipe = new Pipe();
            return pipe;
        }

        public void Add(Calculate c)
        {
            pipe.Enqueue(c);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ProducerConsumer
{
    class Consumer
    {

        public void execute()
        {
            int count = 0;
            while (count < 20)
            {
                Pipe p = Pipe.getPipe();
                if (p.Count != 0)
                {
                    Calculate c = p.Dequeue();
                    Console.WriteLine(c.ToString());
                    count++;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEW_Facade_GameSettings
{
    interface IGameSettingFacade
    {
        void SetGraphicSettings(GraphicsPreferences g);
        void SetSoundSettings(SoundPreferences s);
        void SetGeneralSettings(GamePreferences g, GeneralPreferences ge);
    }

    class DetailedGameSettingsFacade:IGameSettingFacade
    {
        IGraphics gameGraphics;
        ISoundSetting gameSoundSettings;
        IGameGeneralSettingFacade gameGeneralSettings;

        public DetailedGameSettingsFacade(IGraphics g, ISoundSetting s, IGameGeneralSettingFacade gameGeneralSettings)
        {
            gameGraphics = g;
            gameSoundSettings = s;
            this.gameGeneralSettings = gameGeneralSettings;
        }

        public void SetGeneralSettings(GamePreferences g, GeneralPreferences ge)
        {
            gameGeneralSettings.Configure(ge, g);
        }

        public void SetGraphicSettings(GraphicsPreferences g)
        {
            gameGraphics.SetResolution(g.yResolution, g.xResolution);
            gameGraphics.SetFPS(g.fps);
            gameGraphics.SetShadowQuality(g.shadowQuality);
            gameGraphics.SetWaterQuality(g.waterQuality);
            gameGraphics.TextureQuality(g.textureQuality);
            gameGraphics.TestIt();
        }

        public void SetSoundSettings(SoundPreferences s)
        {
            gameSoundSettings.SetDialogueSound(s.dialogueSound);
            gameSoundSettings.SetEnvironmentSound(s.environmentSound);
            gameSoundSettings.SetSoundEffects(s.soundEffects);
            gameSoundSettings.SetTheGeneralSound(s.generalSound);
            gameSoundSettings.TestIt();
        }
    }
    class RestrictedGameSettingsFacade : IGameSettingFacade
    {
        IGraphics gameGraphics;
        ISoundSetting gameSoundSettings;
        IGameGeneralSettingFacade gameGeneralSettings;

        public RestrictedGameSettingsFacade(IGraphics g, ISoundSetting s, IGameGeneralSettingFacade gameGeneralSettings)
        {
            gameGraphics = g;
            gameSoundSettings = s;
            this.gameGeneralSettings = gameGeneralSettings;
        }

        public void SetGeneralSettings(GamePreferences g, GeneralPreferences ge)
        {
            gameGeneralSettings.Configure(ge, g);
        }

        public void SetGraphicSettings(GraphicsPreferences g)
        {
            g = GraphicPreferencesAnalyze(g);

            gameGraphics.SetResolution(g.xResolution, g.yResolution);
            gameGraphics.SetFPS(g.fps);
            gameGraphics.SetShadowQuality(g.shadowQuality);
            gameGraphics.SetWaterQuality(g.waterQuality);
            gameGraphics.TextureQuality(g.textureQuality);
            gameGraphics.TestIt();
        }

        public void SetSoundSettings(SoundPreferences s)
        {
            s = SoundPreferencesAnalyze(s);

            gameSoundSettings.SetDialogueSound(s.dialogueSound);
            gameSoundSettings.SetEnvironmentSound(s.environmentSound);
            gameSoundSettings.SetSoundEffects(s.soundEffects);
            gameSoundSettings.SetTheGeneralSound(s.generalSound);
            gameSoundSettings.TestIt();
        }

        private GraphicsPreferences GraphicPreferencesAnalyze(GraphicsPreferences g)
        {
            GraphicsPreferences preferences = g;

            if (preferences.fps > 30)
                preferences.fps = 30;

            if (preferences.xResolution > 720)
            {
                preferences.xResolution = 720;
            }
            if (preferences.yResolution > 480)
                preferences.yResolution = 480;

            return preferences;
        }

        private SoundPreferences SoundPreferencesAnalyze(SoundPreferences s)
        {
            SoundPreferences preferences = s;

            if (preferences.dialogueSound < 80)
                preferences.dialogueSound = 90;

            if (preferences.generalSound > 50)
                preferences.generalSound = 50;

            return preferences;
        }

    }
}

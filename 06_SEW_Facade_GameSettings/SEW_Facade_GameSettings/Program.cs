﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEW_Facade_GameSettings
{
    class Program
    {
        static void Main(string[] args)
        {
            // only for testing

            SoundPreferences s = new SoundPreferences(92, 50, 75, 100);

            GraphicsPreferences g = new GraphicsPreferences(3840, 2160, 60, EQuality.Low, EQuality.High, EQuality.Medium);

            GamePreferences gamPref = new GamePreferences(150,EDifficulty.Hard);

            GeneralPreferences genPref = new GeneralPreferences(75, EDisplay.Borderless);

            IGameSettingFacade d = new DetailedGameSettingsFacade(new LowGraphicSetting(),new HighSoundQualitySetting(),
                new GameGeneralSettings(new GeneralSettings(), new GameSettings()));
            
            d.SetGraphicSettings(g);

            d.SetSoundSettings(s);

            d.SetGeneralSettings(gamPref, genPref);

            //restricted
            SoundPreferences s2 = new SoundPreferences(92, 50, 75, 100);

            GraphicsPreferences g2 = new GraphicsPreferences(3840, 2160, 60, EQuality.Low, EQuality.High, EQuality.Medium);

            GamePreferences gamPref2 = new GamePreferences(150, EDifficulty.Hard);

            GeneralPreferences genPref2 = new GeneralPreferences(75, EDisplay.Borderless);

            IGameSettingFacade d2 = new RestrictedGameSettingsFacade(new LowGraphicSetting(), new HighSoundQualitySetting(),
                new GameGeneralSettings(new GeneralSettings(), new GameSettings()));

            d2.SetGraphicSettings(g2);

            d2.SetSoundSettings(s2);

            d2.SetGeneralSettings(gamPref2, genPref2);
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Offshore
{
    public interface Account
    {
        double getBalance();
        bool isOverdraftAvailable();
        void credit(double credit);
    }
}

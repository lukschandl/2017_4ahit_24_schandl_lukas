﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_TE
{
    class Person
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {

            #region                                                                                             LVL1
            Console.WriteLine("LVL1:");

            int[] numbers = new int[] { 23, 54, 4, 12, 45, 21, 3, 12, 54, 358, 54, 454, 211, 481, 45, 14, 8 };
            var result1 = numbers
                .Where(x => x > 10 && x < 400)
                .OrderBy(x => x);

            foreach (var num in result1)
            {
                Console.WriteLine($"{num} ,");
            }
            #endregion

            #region                                                                                             LVL2
            Console.WriteLine("\nLVL2:");

            var result2 = numbers
                .Where(x => x%3==0)
                .Distinct()
                .OrderByDescending(x => x);

            foreach (var num in result2)
            {
                Console.WriteLine($"{num} ,");
            }
            #endregion

            #region                                                                                             LVL3
            Console.WriteLine("\nLVL3:");

            int[] numbers1 = new int[] { 28, 54, 4, 32, 23, 21, 3, 12, 54, 358, 90, 454, 43, 481, 45, 14, 65 };
            int[] numbers2 = new int[] { 23, 54, 7, 12, 45, 21, 3, 43, 522, 32, 88, 78, 211, 89, 34, 14, 82 };
            int[] numbers3 = new int[] { 70, 54, 4, 56, 45, 42, 3, 76, 54, 38, 54, 4, 211, 41, 12, 35, 2 };

            var result3 = numbers1.Union(numbers2).Union(numbers3).Where(x => x < 200).Count();

            Console.WriteLine(result3);

            Console.WriteLine();
            #endregion

            #region                                                                                              LVL4
            Console.WriteLine("\nLVL4:");

            List<Person> people = new List<Person>();
            people.Add(new Person { ID = 1, FirstName = "Gregor", LastName = "Beutlin", Country = "Österreich" });
            people.Add(new Person { ID = 2, FirstName = "Heinz", LastName = "Groß", Country = "Deutschland" });
            people.Add(new Person { ID = 3, FirstName = "Jörg", LastName = "Zaun", Country = "Österreich" });
            people.Add(new Person { ID = 4, FirstName = "Michael", LastName = "Schratti", Country = "Deutschland" });
            people.Add(new Person { ID = 5, FirstName = "Stefan", LastName = "Hauser", Country = "Schweiz" });
            people.Add(new Person { ID = 6, FirstName = "Christoph", LastName = "Falz", Country = "Österreich" });
            people.Add(new Person { ID = 7, FirstName = "Julius", LastName = "Pulblus", Country = "England" });
            people.Add(new Person { ID = 8, FirstName = "Heinz", LastName = "Kron", Country = "Deutschland" });
            people.Add(new Person { ID = 9, FirstName = "Ernst", LastName = "Peter", Country = "Österreich" });
            people.Add(new Person { ID = 10, FirstName = "Peter", LastName = "Griffin", Country = "England" });
            people.Add(new Person { ID = 11, FirstName = "Frank", LastName = "Jaker", Country = "England" });

            var result4 = people.GroupBy(x => x.Country).OrderByDescending(x => x.Count());
            foreach (var group in result4)
            {
                Console.WriteLine(group.Key + " " + group.Count());
                foreach (var person in group)
                {
                    Console.WriteLine("     " + person.FirstName + " " + person.LastName);
                }
            }
            #endregion

            Console.ReadKey();

        }
    }
}

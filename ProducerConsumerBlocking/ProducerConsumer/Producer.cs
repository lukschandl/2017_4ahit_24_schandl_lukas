﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;

namespace ProducerConsumer
{
    class Producer
    {
        public void execute()
        {
            int count = 0;
            while(count < 20)
            {
                Calculate c = new Calculate();
                //Pipe p = Pipe.getPipe();
                Pipe p = Pipe.getPipe();
                p.Add(c);
                count++;
                Thread.Sleep(100);
            }
        }
    }
}

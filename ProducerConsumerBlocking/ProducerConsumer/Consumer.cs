﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;

namespace ProducerConsumer
{
    class Consumer
    {

        public void execute()
        {
            int count = 0;
            while (count < 20)
            {
                //Pipe p = Pipe.getPipe();
                Pipe p = Pipe.getPipe();
                if (p.getCollection().Count != 0)
                {
                    Calculate c = p.getCollection().Take();
                    Console.WriteLine(c.ToString());
                    count++;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Maresch_Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public static class Exercises
    {
        /// <summary>
        /// Exercise 1
        /// </summary>
        /// <param name="userFile">Path to the XML file containing the users</param>
        /// <returns>Users as an IEnumerable of Tuples (lists of values) which contain two strings (name and type)</returns>
        public static IEnumerable<(string Name, string Type)> Exercise1(string userFile)
        {
            var usersStartingWithTOrderedBySurnameDescending =
                from user in XDocument.Load(userFile)
                                      .Element("users")
                                      .Elements()
                where ((string)user.Attribute("name")).StartsWith("T")
                orderby ((string)user.Attribute("name")).Split(' ')[1] descending // Orders by surname in descending order
                select new
                {
                    Name = (string)user.Attribute("name"),
                    Type = (string)user.Attribute("type")
                };

            // Maps the result to an IEnumerable of Tuples which contains the name and the type per user
            return usersStartingWithTOrderedBySurnameDescending.Select(user => (user.Name, user.Type));
        }

        /// <summary>
        /// Exercise 2
        /// </summary>
        /// <param name="userFile">Path to the XML file containing the users</param>
        /// <returns>Users as an IEnumerable of Tuples (lists of values) which contain two strings (name and type)</returns>
        public static IEnumerable<(string Name, string Type)> Exercise2(string userFile)
        {
            var typesInDescendingOrderWithFirstUserInAlphabeticalOrder = XDocument.Load(userFile)
                .Element("users")
                .Elements()
                .OrderByDescending(user => (string)user.Attribute("type"))
                .Select(user => (string)user.Attribute("type")) // Gets all types
                .Distinct() // Filters out duplicates
                .Select(type => new
                {
                    Name = XDocument.Load(userFile) // Sequence no longer contains the names -> another LINQ query
                        .Element("users")
                        .Elements()
                        .Where(user => (string)user.Attribute("type") == type) // Gets all users having the currently evaluated type
                        .OrderBy(user => (string)user.Attribute("name"))
                        .Select(user => (string)user.Attribute("name"))
                        .First(), // Selects the first user (which is the first one in alphabetical order due to the call to OrderBy)
                    Type = type
                });
            // Alternative solution
            var typesInDescendingOrderWithFirstUserInAlphabeticalOrder1 = XDocument.Load(userFile)
                .Element("users")
                .Elements()
                .GroupBy(user => (string)user.Attribute("type")) // Groups by type
                .Select(users => users.OrderBy(user => (string)user.Attribute("name")).First()) // Orders the groups by their names and selects the first user of each group, who now is the first user in alphabetical order
                .OrderByDescending(user => (string)user.Attribute("type")) // Orders by types in descending order
                .Select(user => new
                {
                    Name = (string)user.Attribute("name"),
                    Type = (string)user.Attribute("type")
                });

            // Maps the result to an IEnumerable of Tuples which contains the name and the type per user
            return typesInDescendingOrderWithFirstUserInAlphabeticalOrder.Select(user => (user.Name, user.Type));
        }

        /// <summary>
        /// Exercise 3
        /// </summary>
        /// <returns>Name of the file the users are saved to</returns>
        public static string Exercise3()
        {
            string filename = "DevelopersAndManagers.xml";
            var SampleData = new[]
            {
                new {Name = "Tim Denton", Type = "Tester"},
                new {Name = "Tara Totue", Type = "Tester"},
                new {Name = "Sarah Trotter", Type = "Developer"},
                new {Name = "John Dahla", Type = "Developer"},
                new {Name = "Mary Malcopp", Type = "Manager"},
                new {Name = "Colin Carton", Type = "Customer"}
            }.ToList();

            var developersAndManagers = new XElement("users", // Root element                               
                from user in SampleData
                where user.Type == "Developer" || // Filters users based on their type
                      user.Type == "Manager"
                select new XElement("user", user.Name, new XAttribute("type", user.Type))); // Generates a new XElement for each user matching the criterias

            using (StreamWriter sw = new StreamWriter(filename, false))
            {
                sw.Write(developersAndManagers);
            }

            return filename;
        }

        /// <summary>
        /// Exercise 4
        /// </summary>
        /// <returns>Name of the file the users are saved to</returns>
        public static string Exercise4()
        {
            string filename = "TypesWithNames.xml";
            var SampleData = new[]
            {
                new {Name = "Tim Denton", Type = "Tester"},
                new {Name = "Tara Totue", Type = "Tester"},
                new {Name = "Sarah Trotter", Type = "Developer"},
                new {Name = "John Dahla", Type = "Developer"},
                new {Name = "Mary Malcopp", Type = "Manager"},
                new {Name = "Colin Carton", Type = "Customer"}
            }.ToList();

            var typesWithNames = new XElement("types", // Root element 
                SampleData.Select(user => user.Type) // Gets all types
                    .Distinct() // Filters out duplicates
                    .Select(type => new XElement(type.ToLower(), SampleData // Generates a new XElement for each unique type
                            .Where(user => user.Type == type) // Filters out users of other types
                            .OrderBy(user => user.Name)
                            .Select(user => new XElement("name", user.Name))) // Generates a new XElement for each user (in alphabetical order, due to the call to OrderBy)
                    ));
            // Alternative solution
            var typesWithNames1 = new XElement("types", // Root element 
                SampleData.GroupBy(user => user.Type) // Groups by type
                    .Select(users => users.OrderBy(user => user.Name)) // Orders the groups by name
                    .Select(users => new XElement(users.First() // Generates a new XElement for each type
                                                       .Type
                                                       .ToLower(), users.Select(user => new XElement("name", user.Name))) // Generates a new XElement for each user
                    ));

            using (StreamWriter sw = new StreamWriter(filename, false))
            {
                sw.Write(typesWithNames);
            }

            return filename;
        }
    }
}

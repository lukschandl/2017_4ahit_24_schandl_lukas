﻿using System;
using System.Linq;
using System.Xml.Linq;
using Xunit;
using Maresch_Linq;

namespace Maresch_Linq.Tests
{
    public class UnitTests
    {
            [Fact]
            public void TestExercise1()
            {
                var actualResult = Exercises.Exercise1("../../../../Beispiel/Users.xml");
                var desiredResult = new[]
                {
                new {Name = "Tara Totue", Type = "Tester"},
                new {Name = "Tim Denton", Type = "Tester"}
            }.ToList();

                Assert.StrictEqual(actualResult.Count(), desiredResult.Count);
                for (int i = 0; i < actualResult.Count(); i++)
                {
                    Assert.Equal(actualResult.ElementAt(i).Name, desiredResult.ElementAt(i).Name);
                    Assert.Equal(actualResult.ElementAt(i).Type, desiredResult.ElementAt(i).Type);
                }
            }

            [Fact]
            public void TestExercise2()
            {
                var actualResult = Exercises.Exercise2("../../../../Beispiel/Users.xml");
                var desiredResult = new[]
                {
                new {Name = "Tara Totue", Type = "Tester"},
                new {Name = "Mary Malcopp", Type = "Manager"},
                new {Name = "John Dahla", Type = "Developer"},
                new {Name = "Colin Carton", Type = "Customer"}
            }.ToList();

                Assert.StrictEqual(actualResult.Count(), desiredResult.Count);
                for (int i = 0; i < actualResult.Count(); i++)
                {
                    Assert.Equal(actualResult.ElementAt(i).Name, desiredResult.ElementAt(i).Name);
                    Assert.Equal(actualResult.ElementAt(i).Type, desiredResult.ElementAt(i).Type);
                }
            }

            [Fact]
            public void TestExercise3()
            {
                var actualResult = XDocument.Load("../../../../Beispiel/" + Exercises.Exercise3())
                    .Element("users")
                    .Elements()
                    .Select(user => new
                    {
                        Name = user.Value,
                        Type = (string)user.Attribute("type")
                    });
                var desiredResult = new[]
                {
                new {Name = "Sarah Trotter", Type = "Developer"},
                new {Name = "John Dahla", Type = "Developer"},
                new {Name = "Mary Malcopp", Type = "Manager"}
            }.ToList();

                Assert.StrictEqual(actualResult.Count(), desiredResult.Count);
                for (int i = 0; i < actualResult.Count(); i++)
                {
                    Assert.Equal(actualResult.ElementAt(i).Name, desiredResult.ElementAt(i).Name);
                    Assert.Equal(actualResult.ElementAt(i).Type, desiredResult.ElementAt(i).Type);
                }
            }

            [Fact]
            public void TestExercise4()
            {
                var actualResult = XDocument.Load("../../../../Beispiel/" + Exercises.Exercise4())
                    .Element("types")
                    .Elements()
                    .GroupBy(type => type.Name)
                    .SelectMany(type => type.Elements().Select(user => new
                    {
                        Name = user.Value,
                        Type = type.Key.ToString()
                    }))
                    .OrderBy(user => user.Name);
                var desiredResult = new[]
                {
                new {Name = "Tim Denton", Type = "Tester"},
                new {Name = "Tara Totue", Type = "Tester"},
                new {Name = "Sarah Trotter", Type = "Developer"},
                new {Name = "John Dahla", Type = "Developer"},
                new {Name = "Mary Malcopp", Type = "Manager"},
                new {Name = "Colin Carton", Type = "Customer"}
            }.OrderBy(user => user.Name)
                .ToList();

                Assert.StrictEqual(actualResult.Count(), desiredResult.Count);
                for (int i = 0; i < actualResult.Count(); i++)
                {
                    Assert.Equal(actualResult.ElementAt(i).Name, desiredResult.ElementAt(i).Name);
                    Assert.Equal(actualResult.ElementAt(i).Type, desiredResult.ElementAt(i).Type.ToLower());
                }
            }
        }
    }
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactorySample.PcParts
{
    class ExpensiveHardDisk: IHardDisk
    {
        public string GiveHardDisk()
        {
            return "ExpensiveHardDisk";
        }
    }
}

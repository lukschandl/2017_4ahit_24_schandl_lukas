﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractFactorySample.PcParts;

namespace AbstractFactorySample
{
    class BuildCheapPc : IPcFactory
    {
        public void GetProcessor()
        {
            Console.WriteLine(new CheapProcessor().GiveProcessor());
        }

        public void GetHardDisk()
        {
            Console.WriteLine(new CheapHardDisk().GiveHardDisk());
        }

        public void GetRam()
        {
            Console.WriteLine(new CheapRam().GiveRam());
        }

        public void GetMonitor()
        {
            Console.WriteLine(new CheapMonitor().GiveMonitor());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactorySample
{
    class Program
    {
        static void Main(string[] args)
        {
            IPcFactory pcFactory = new BuildCheapPc();
            Console.WriteLine("cheap");
            pcFactory.GetHardDisk();
            pcFactory.GetMonitor();
            pcFactory.GetProcessor();
            pcFactory.GetRam();
            Console.WriteLine();
            pcFactory = new BuildExpensivePc();
            Console.WriteLine("expensive");
            pcFactory.GetHardDisk();
            pcFactory.GetMonitor();
            pcFactory.GetProcessor();
            pcFactory.GetRam();
            Console.WriteLine();
            pcFactory = new BuildMemoryPc();
            Console.WriteLine("memory");
            pcFactory.GetHardDisk();
            pcFactory.GetMonitor();
            pcFactory.GetProcessor();
            pcFactory.GetRam();
        }
    }
}

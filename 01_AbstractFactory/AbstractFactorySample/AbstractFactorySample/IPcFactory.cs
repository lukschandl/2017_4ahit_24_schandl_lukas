﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactorySample
{
    interface IPcFactory
    {
        void GetProcessor();
        void GetHardDisk();
        void GetRam();
        void GetMonitor();

    }
}

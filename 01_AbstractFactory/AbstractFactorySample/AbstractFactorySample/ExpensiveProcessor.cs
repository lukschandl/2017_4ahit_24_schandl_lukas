﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactorySample.PcParts
{
    class ExpensiveProcessor : IProcessor
    {
        public string GiveProcessor()
        {
            return "ExpensiveProcessor";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactorySample.PcParts
{
    class CheapMonitor : IMonitor
    {
        public string GiveMonitor()
        {
            return "CheapMonitor";
        }
    }
}

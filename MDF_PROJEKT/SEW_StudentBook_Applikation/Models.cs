﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data.Entity.ModelConfiguration;

namespace BookOrderStudentApplication
{

    public interface IEntity
    {
        int ID { get; }
    }
    [Table("Books")]
    public class Book : IEntity
    { 
        public Book()
        {
            Orders = new HashSet<Order>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        [StringLength(50)]
        public string Author { get; set; }

        public decimal Price { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
   
    [Table("Orders")]
    public class Order:IEntity
    {

        public Order(int id, int b_id, int s_id, DateTime orderDate)
        {
            this.ID = id;
            this.Book_ID = b_id;
            this.Student_ID = s_id;
            this.orderDate = orderDate;
        }
        public Order()
        {

        }

        public int ID { get; set; }

        [ForeignKey("Book")]
        public int Book_ID { get; set; }
        [ForeignKey("Student")]
        public int Student_ID { get; set; }

        public DateTime orderDate { get; set; }

        public virtual Book Book { get; set; }

        public virtual Student Student { get; set; }
    }
    [Table("Students")]
    public class Student : IEntity
    {
        public Student()
        {
            Orders = new HashSet<Order>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        public int Age { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }

    public class BookMap:EntityTypeConfiguration<Book>
    {
        public BookMap() {
            HasMany(t => t.Orders).WithRequired(t => t.Book).HasForeignKey(t => t.Book_ID);
        }
    }

    public class StudentMap : EntityTypeConfiguration<Student>
    {
        public StudentMap() {
            HasMany(t => t.Orders).WithRequired(t => t.Student).HasForeignKey(t => t.Student_ID);
        }
    }

    public class OrderMap : EntityTypeConfiguration<Order>
    {
        public OrderMap() {
            
        }
    }
}

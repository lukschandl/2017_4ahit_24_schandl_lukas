﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookOrderStudentApplication
{
   
    public class StudentViewModel:INotifyPropertyChanged
    {
        public StudentViewModel()
        {
            Students = GetStudentList();
        }

        ObservableCollection<Student> GetStudentList()
        {
            StudentRepository sr = new StudentRepository();
            List<Student> slist = sr.GetEntities();
            ObservableCollection<Student> ob = new ObservableCollection<Student>();
            foreach (Student s in slist)
            {
                ob.Add(s);
            }
            return ob;
        }

        public void Refresh()
        {
            Students=GetStudentList();
        }

        private ObservableCollection<Student> students;
        public ObservableCollection<Student> Students
        {
            get { return students; }
            set
            {
                students = value;
                RaiseChange("Students");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaiseChange(string PropertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
    public class BookViewModel : INotifyPropertyChanged
    {
        public BookViewModel()
        {
            Books = GetBookList();
        }

        ObservableCollection<Book> GetBookList()
        {
            BookRepository br = new BookRepository();
            List<Book> blist = br.GetEntities();
            ObservableCollection<Book> ob = new ObservableCollection<Book>();
            foreach (Book b in blist)
            {
                ob.Add(b);
            }
            return ob;
        }

        public void Refresh()
        {
            Books = GetBookList();
        }

        private ObservableCollection<Book> books;
        public ObservableCollection<Book> Books
        {
            get { return books; }
            set
            {
                books = value;
                RaiseChange("Books");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaiseChange(string PropertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
    }


    public class StudentBookViewModel:INotifyPropertyChanged
    {
        public StudentBookViewModel()
        {
            StudentBooks = GetStudentBookList();
        }

        ObservableCollection<Order> GetStudentBookList()
        {
            OrderRepository orep = new OrderRepository();
            ObservableCollection<Order> obc = new ObservableCollection<Order>();
            List<Order> orders = orep.GetEntities();
            foreach (Order o in orders)
            {
                obc.Add(o);
            }
            return obc;
        }

        public void Refresh()
        {
            StudentBooks = GetStudentBookList();
        }

        private ObservableCollection<Order> studentbooks;
        public ObservableCollection<Order> StudentBooks
        {
            get { return studentbooks; }
            set
            {
                studentbooks = value;
                RaiseChange("StudentBooks");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaiseChange(string PropertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
    }
}

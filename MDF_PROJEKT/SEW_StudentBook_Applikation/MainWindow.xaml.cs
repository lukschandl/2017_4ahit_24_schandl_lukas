﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Linq;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace BookOrderStudentApplication
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }
       
        public void SetUpBookTable()
        {
            string xmlConnection = "books.xml";
            var xmlfile = XDocument.Load(xmlConnection);
            var rootElementDesc = xmlfile.Root.Descendants("book");
            var books = from book in rootElementDesc
                        select new
                        {
                            author = book.Element("author").Value,
                            title = book.Element("title").Value,
                            price = book.Element("price").Value,
                        };
            SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = D:\4AHIT\SEW\21_StudentBook_Applikation\SEW_StudentBook_Applikation\DB.mdf; Integrated Security = True; Connect Timeout = 30");
            con.Open();
            var commandstr = "IF NOT EXISTS (SELECT NAME FROM sysobjects WHERE name='Books') CREATE TABLE Books(ID INT IDENTITY(1,1) NOT NULL, Title varchar(50) NOT NULL, Author varchar(50) NOT NULL, Price DECIMAL NOT NULL, PRIMARY KEY(ID))";
            using (SqlCommand command = new SqlCommand(commandstr, con))
            { command.ExecuteNonQuery(); }

            string commandstr2;

            foreach (var b in books)
            {
                commandstr2 = "INSERT INTO Books (Title, Author, Price) VALUES ('" + b.title + "','" + b.author + "'," + String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}", b.price) + ")";
                using (SqlCommand command = new SqlCommand(commandstr2, con))
                { command.ExecuteNonQuery(); }
            }

            con.Close();
            MessageBox.Show("Executed");
        }

        public void SetUpStudentTable()
        {
            SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = D:\4AHIT\SEW\21_StudentBook_Applikation\SEW_StudentBook_Applikation\DB.mdf; Integrated Security = True; Connect Timeout = 30");
            con.Open();
            var commandstr = "IF NOT EXISTS (SELECT NAME FROM sysobjects WHERE name='Students') CREATE TABLE Students(ID INT IDENTITY(1,1) NOT NULL, FirstName varchar(50) NOT NULL, LastName varchar(50) NOT NULL, Age Integer NOT NULL, PRIMARY KEY(ID))";
            using (SqlCommand command = new SqlCommand(commandstr, con))
            { command.ExecuteNonQuery(); }

            string commandstr2 = "INSERT INTO Students(FirstName, LastName, Age) VALUES('Franz','Huber',19)";
            using (SqlCommand command = new SqlCommand(commandstr2, con))
            { command.ExecuteNonQuery(); }
            con.Close();
            MessageBox.Show("2");
        }

        public void SetUpOrderTable()
        {
            List<Order> orderList = new List<Order>();
            Order s1 = new Order(1, 1, 1, DateTime.Now);
            Order s2 = new Order(1, 2, 1, DateTime.Now);
            Order s3 = new Order(1, 3, 1, DateTime.Now);
            Order s4 = new Order(1, 4, 1, DateTime.Now);
            orderList.Add(s1);
            orderList.Add(s2);
            orderList.Add(s3);
            orderList.Add(s4);

            SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = D:\4AHIT\SEW\21_StudentBook_Applikation\SEW_StudentBook_Applikation\DB.mdf; Integrated Security = True; Connect Timeout = 30");
            con.Open();
            var commandstr = "IF NOT EXISTS (SELECT NAME FROM sysobjects WHERE name='Orders') CREATE TABLE Orders(ID INT IDENTITY(1,1) NOT NULL, Book_ID INTEGER NOT NULL, Student_ID INTEGER NOT NULL, orderDate DateTime NOT NULL, FOREIGN KEY (Book_ID) REFERENCES Books(ID), FOREIGN KEY (Student_ID) REFERENCES Students(ID), PRIMARY KEY(ID))";
            using (SqlCommand command = new SqlCommand(commandstr, con))
            { command.ExecuteNonQuery(); }
            string commandstr2;
            foreach (Order s in orderList)
            {
                commandstr2 = "INSERT INTO Orders (Book_ID, Student_ID, orderDate) VALUES (" + s.Book_ID + "," + s.Student_ID + ",'" + DateTime.Now + "')";
                using (SqlCommand command = new SqlCommand(commandstr2, con))
                { command.ExecuteNonQuery(); }
            }

            con.Close();
            MessageBox.Show("3");
        }

        private void btn_bookshow_Click(object sender, RoutedEventArgs e)
        {
            BookView bv = new BookView();
            bv.Show();
        }

        private void btn_studentshow_Click(object sender, RoutedEventArgs e)
        {
            StudentView sv = new StudentView();
            sv.Show();
        }

        private void btn_ordershow_Click(object sender, RoutedEventArgs e)
        {
            OrderView ov = new OrderView();
            ov.Show();
        }
    }
}

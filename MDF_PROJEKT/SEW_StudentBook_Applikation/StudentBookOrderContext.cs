﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookOrderStudentApplication
{
    class StudentBookOrderContext:DbContext
    {
        public StudentBookOrderContext() : base(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = D:\4AHIT\SEW\21_StudentBook_Applikation\SEW_StudentBook_Applikation\DB.mdf; Integrated Security = True; Connect Timeout = 30") { }
        public DbSet<Book> Book { get; set; }
        public DbSet<Student> Student { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}

﻿using SEW_StudentBook_Applikation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookOrderStudentApplication
{
    /// <summary>
    /// Interaktionslogik für BookView.xaml
    /// </summary>
    public partial class BookView : Window
    {
        public BookRepository br;
        string title;
        decimal price;
        string author;

        public BookView()
        {
            InitializeComponent();
            br = new BookRepository();
        }

        private void listbox_selectItem(object sender, MouseButtonEventArgs e)
        {
            Book b = br.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            txt_title.Text = b.Title;
            txt_price.Text = Convert.ToString(b.Price);
            txt_author.Text = b.Author;

            title = txt_title.Text;
            price = Convert.ToDecimal(txt_price.Text);
            author = txt_author.Text;
        }
        public void Insert()
        {
            title = txt_title.Text;
            price = Convert.ToDecimal(txt_price.Text);
            author = txt_author.Text;

            Book b = new Book();
            b.Price = price;
            b.Title = title;
            b.Author = author;

            br.Insert(b);
            BookViewModel t = (BookViewModel)this.DataContext;
            t.Refresh();
        }

        public void Update()
        {
            title = txt_title.Text;
            price = Convert.ToDecimal(txt_price.Text);
            author = txt_author.Text;

            Book b = br.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            b.Price = price;
            b.Title = title;
            b.Author = author;
          

            br.Update(b);
            BookViewModel t = (BookViewModel)this.DataContext;
            ObservableCollection<Book> k = t.Books;
            foreach(Book b2 in k)
            {
                if (b.ID == b2.ID)
                {
                    b2.Author = b.Author;
                    b2.Title = b.Title;
                    b2.Price = b.Price;
                }
            }
            t.RaiseChange("Students");
        }

        private void btn_create_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RelayCommand r = new RelayCommand(Insert);
                if (r.CanExecute(new Object()))
                {
                    r.Execute(new Object());
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RelayCommand r = new RelayCommand(Update);
                if (r.CanExecute(new Object()))
                {
                    r.Execute(new Object());
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RelayCommand r = new RelayCommand(Delete);
                if (r.CanExecute(new Object()))
                {
                    r.Execute(new Object());
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        public void Delete()
        {
            Book b = br.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            br.Delete(b.ID);
            BookViewModel t= (BookViewModel)this.DataContext;
            t.Refresh();
        }

        private void btn_search_click(object sender, RoutedEventArgs e)
        {
            FilterResults fr = new FilterResults();
            fr.BookFilter(br.SearchForBook(txt_searchForTitle.Text));
            fr.Show();
        }
    }
}

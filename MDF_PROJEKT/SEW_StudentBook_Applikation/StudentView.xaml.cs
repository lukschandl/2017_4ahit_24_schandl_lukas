﻿using SEW_StudentBook_Applikation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookOrderStudentApplication
{
    /// <summary>
    /// Interaktionslogik für StudentView.xaml
    /// </summary>
    public partial class StudentView : Window
    {
        public StudentRepository sr=new StudentRepository();
        string firstname;
        string lastname;
        int age;
        public StudentView()
        {
            InitializeComponent();
        }
        private void listbox_selectItem(object sender, SelectionChangedEventArgs e)
        {
           
        }
        private void btn_create_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                RelayCommand r = new RelayCommand(Insert);
                if (r.CanExecute(new Object()))
                {
                    r.Execute(new Object());
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RelayCommand r = new RelayCommand(Update);
                if (r.CanExecute(new Object()))
                {
                    r.Execute(new Object());
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RelayCommand r = new RelayCommand(Delete);
                if (r.CanExecute(new Object()))
                {
                    r.Execute(new Object());
                }
            }
            catch
            {
                MessageBox.Show("Error");
            }
        }

        private void btn_findTeenagers_Click(object sender, RoutedEventArgs e)
        {
            FilterResults fr = new FilterResults();
            fr.StudentFilter(sr.findTeenagers());
            fr.Show();
        }

        
        public void Insert()
        {
            lastname = txt_lastname.Text;
            age = Convert.ToInt32(txt_age.Text);
            firstname = txt_firstname.Text;

            Student s = new Student();
            s.Age = age;
            s.FirstName = firstname;
            s.LastName = lastname;

            sr.Insert(s);
            StudentViewModel t = (StudentViewModel)this.DataContext;
            t.Refresh();
        }

        private void listbox_selectedItem(object sender, MouseButtonEventArgs e)
        {
            DataGrid d = sender as DataGrid;
            Student row = (Student)listbox_entities.SelectedItem;

            int i = row.ID;
            Student s = sr.GetEntityByID(i);
            txt_firstname.Text = s.FirstName;
            txt_age.Text = Convert.ToString(s.Age);
            txt_lastname.Text = s.LastName;

            lastname = txt_lastname.Text;
            age = Convert.ToInt32(txt_age.Text);
            firstname = txt_firstname.Text;
        }

        public void Update()
        {
            lastname = txt_lastname.Text;
            age = Convert.ToInt32(txt_age.Text);
            firstname = txt_firstname.Text;
            int i = Convert.ToInt32(listbox_entities.SelectedValue.ToString());
            Student s = sr.GetEntityByID(i);
            s.Age = age;
            s.FirstName = firstname;
            s.LastName = lastname;


            sr.Update(s);
            StudentViewModel t = (StudentViewModel)this.DataContext;
            ObservableCollection<Student> k = t.Students;
            foreach (Student s2 in k)
            {
                if (s.ID == s2.ID)
                {
                    s2.LastName = s.LastName;
                    s2.FirstName = s.FirstName;
                    s2.Age = s.Age;
                }
            }
            t.RaiseChange("Students");
        }

        public void Delete()
        {
            Student s = sr.GetEntityByID(Convert.ToInt32(listbox_entities.SelectedValue.ToString()));
            sr.Delete(s.ID);
            StudentViewModel t = (StudentViewModel)this.DataContext;
            t.Refresh();
        }
    }
}

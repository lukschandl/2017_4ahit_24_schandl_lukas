﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookOrderStudentApplication
{
    interface IRepository<T>:IDisposable
    {
        List<T> GetEntities();
        T GetEntityByID(int id);
        void Insert(T entity);
        void Delete(int id);
        void Update(T entity);
        void Save();
    }

   public class BookRepository : IRepository<Book>
    {
        public List<Book> GetEntities()
        {
            List<Book> l = new List<Book>();
            using (var context=new StudentBookOrderContext())
            {
                l = context.Book.Select(x => x).ToList();
            }
            return l;
        }

        public Book GetEntityByID(int id)
        {
            Book b;
            using (var context = new StudentBookOrderContext())
            {
                b = context.Book.First(x=>x.ID==id);
            }
            return b;
        }

        public void Insert(Book b)
        {
            using (var context = new StudentBookOrderContext())
            {
                context.Book.Add(b);
                context.SaveChanges();
            }
        }

        public void Update(Book b)
        {
            using (var context = new StudentBookOrderContext())
            {
                context.Entry(b).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

       

        public void Delete(int id)
        {
            using (var context = new StudentBookOrderContext())
            {
                var orders = context.Orders.Where(x => x.Book_ID == id).Select(x => x);
                foreach (Order item in orders)
                {
                    context.Orders.Remove(item);
                }
                Book bm = context.Book.Find(id);
                context.Book.Remove(bm);
                context.SaveChanges();
            }
        }
        public List<Book> SearchForBook(string searchstring)
        {
            List<Book> l = new List<Book>();
            using (var context = new StudentBookOrderContext())
            {
                l = context.Book.Where(x => x.Title.Contains(searchstring)).Select(x => x).ToList();
            }
            return l;
        }

        public void Save()
        {
            using (var context = new StudentBookOrderContext())
            {
                context.SaveChanges();
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (var context = new StudentBookOrderContext())
                    {
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public class StudentRepository : IRepository<Student>
    {
        public List<Student> GetEntities()
        {
            List<Student> l = new List<Student>();
            using (var context = new StudentBookOrderContext())
            {
                l = context.Student.Select(x => x).ToList();
            }
            return l;
        }

        public Student GetEntityByID(int id)
        {
            Student sm = new Student();
            using (var context = new StudentBookOrderContext())
            {
                sm = context.Student.First(x=>x.ID==id);
            }
            return sm;
        }

        public List<Student> findTeenagers()
        {
            List<Student> sl;
            using (var context = new StudentBookOrderContext())
            {
                sl = context.Student.Where(x=>x.Age>=13&&x.Age<=19).Select(x=>x).ToList();
            }
            return sl;
        }

        public void Insert(Student sm)
        {
            using (var context = new StudentBookOrderContext())
            {
                try
                {
                    context.Student.Add(sm);
                    context.SaveChanges();
                }
                catch
                {
                    
                }
            
                
            }
        }

        public void Update(Student sm)
        {
            using (var context = new StudentBookOrderContext())
            {
                context.Entry(sm).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new StudentBookOrderContext())
            {
                var orders = context.Orders.Where(x => x.Student_ID == id).Select(x => x);       
                foreach(Order o in orders)
                {
                    context.Orders.Remove(o);
                }
                Student sm = context.Student.First(x=>x.ID==id);
                context.Student.Remove(sm);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = new StudentBookOrderContext())
            {
                context.SaveChanges();
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (var context = new StudentBookOrderContext())
                    {
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public class OrderRepository : IRepository<Order>
    {
        public List<Order> GetEntities()
        {
            List<Order> l = new List<Order>();
            using (var context = new StudentBookOrderContext())
            {
                l = context.Orders.Select(x => x).ToList();
            }
            return l;
        }

        public Order GetEntityByID(int id)
        {
            Order sbm;
            using (var context = new StudentBookOrderContext())
            {
                sbm = context.Orders.First(x=>x.ID==id);
            }
            return sbm;
        }

        public void Insert(Order sbm)
        {
            using (var context = new StudentBookOrderContext())
            {
                context.Orders.Add(sbm);
                context.SaveChanges();
            }
        }

        public void Update(Order sbm)
        {
            using (var context = new StudentBookOrderContext())
            {
                context.Entry(sbm).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new StudentBookOrderContext())
            {
                Order sbm = context.Orders.First(x=>x.ID==id);
                context.Orders.Remove(sbm);
                context.SaveChanges();
            }
        }

        public void Save()
        {
            using (var context = new StudentBookOrderContext())
            {
                context.SaveChanges();
            }
        }

        public List<Order> getOldestOrders()
        {
            List<Order> oList;
            using (var context = new StudentBookOrderContext())
            {
                oList = context.Orders.Where(x => x.orderDate == context.Orders.Max(d => d.orderDate)).ToList();
            }
            return oList;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    using (var context = new StudentBookOrderContext())
                    {
                        context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

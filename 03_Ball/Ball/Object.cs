﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace Ball
{
  public abstract class Object
    {
        public int counter = 0;
        public abstract Shape getShape();
        public abstract void setShape();

        double speed;

        public void Mousedown(object sender, EventArgs e)
        {
            counter++;
        }

        protected Shape renderShape = null;
        protected RadialGradientBrush brush ;
    }

    class myEllipse : Object
    {
        public override Shape getShape()
        {
            return renderShape;
        }

        public override void setShape()
        {
            renderShape = new Ellipse() { Height = 40, Width = 40 };
            renderShape.MouseDown += new MouseButtonEventHandler(Mousedown);
            brush.GradientStops.Add(new GradientStop());
            renderShape.Fill = Brushes.Blue;
            renderShape.Name = "Ellispe";
            Canvas.SetLeft(renderShape, 100);
            Canvas.SetTop(renderShape, 100);


        }
    }
}

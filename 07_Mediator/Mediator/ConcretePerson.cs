﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class ConcretePerson : Person
    {

        public ConcretePerson(string name, IDistributor distributor) : base(name, distributor)
        {

        }
    }
}

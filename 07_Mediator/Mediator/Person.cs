﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
   public abstract class Person
    {
        public List<string> chat;
        protected IDistributor distributor;
        public string Name;

        public Person(string name, IDistributor distributor)
        {
            chat = new List<string>();
            this.Name = name;
            this.distributor = distributor;
        }

        public void Send(Person to, string message) 
        {
            distributor.Send(this, to, message);
            chat.Add("You to " + to.Name + ": " + message);
        }

        public void Notify(Person from, string message) 
        {
            chat.Add(from.Name + " to you: " + message);
        }
    }
}

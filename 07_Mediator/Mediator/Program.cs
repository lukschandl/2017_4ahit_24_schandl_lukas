﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            IDistributor distributor = new ConcreteDistributor();

            Person schandl = new ConcretePerson("schandl", distributor);
            Person huhitzer = new ConcretePerson("huhitzer", distributor);
            Person naber = new ConcretePerson("nabgergaberjazz", distributor);

            schandl.Send(huhitzer, "SUP");
            huhitzer.Send(schandl, "YA KNOW");
            schandl.Send(naber, "JAZZ");
            naber.Send(schandl, "NABER");

            foreach (string m in naber.chat) 
            {
                Console.WriteLine(m);
            }
            Console.ReadKey();
        }
    }
}

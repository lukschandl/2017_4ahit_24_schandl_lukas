﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class RunningQueueCustomer
    {
        public void run()
        {
            while (true)
            {
                MyThread m = new MyThread();
                if (RunningQueue.getInstance().TryTake(out m))
                {
                    m.nextState();

                    if (m.getState().GetType() == new FinishedState().GetType())
                    {
                        m.nextState();
                    }
                    else if (m.getState().GetType() == new ReadyState().GetType())
                    {
                        ReadyQueue.getInstance().TryAdd(m);
                    }
                    else if (m.getState().GetType() == new BlockState().GetType())
                    {
                        BlockQueue.getInstance().TryAdd(m);
                    }
                    else
                    {
                        RunningQueue.getInstance().TryAdd(m);
                    }
                }
            }
        }
    }
}

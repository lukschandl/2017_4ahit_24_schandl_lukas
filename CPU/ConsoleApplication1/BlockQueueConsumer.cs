﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace ConsoleApplication1
{
    class BlockQueueCustomer
    {
        public void run()
        {
            while (true)
            {
                MyThread m = new MyThread();
                if (BlockQueue.getInstance().TryTake(out m))
                {
                    m.nextState();
                    ReadyQueue.getInstance().TryAdd(m);
                }
            }
        }
    }
}

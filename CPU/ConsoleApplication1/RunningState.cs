﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class RunningState : IState
    {
        private static Random random = new Random();

        public IState changeState(MyThread mt)
        {
            Console.WriteLine(toString() + " " + mt.getName());

            int i = random.Next(0, 100);

            Console.WriteLine("Pivot: " + i);
            if (i >= 0 && i <= 9)
            {
                return new FinishedState();
            }
            else if (i >= 10 && i <= 39)
            {
                return new BlockState();
            }
            else if (i >= 40 && i <= 99)
            {
                return new ReadyState();
            }

            return this;
        }

        public string toString()
        {
            return "Running state:";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class ReadyState : IState
    {
        public IState changeState(MyThread mt)
        {
            Console.WriteLine(toString() + " " + mt.getName());
            return new RunningState();
        }

        public string toString()
        {
            return "Ready state:";
        }
    }
}

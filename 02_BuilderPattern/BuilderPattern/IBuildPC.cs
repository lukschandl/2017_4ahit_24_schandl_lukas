﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    interface IBuildPC
    {
        void BuildMonitor();
        void BuildProzessor();
        void BuildHarddisk();
        void BuildRam();
        
        Computer MyPC { get; }

    }
}

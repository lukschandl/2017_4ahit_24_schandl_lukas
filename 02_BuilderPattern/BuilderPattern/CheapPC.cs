﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class CheapPC: IBuildPC
    {
        Computer mypc;

        public CheapPC ()
        {
            mypc = new Computer("Cheap PC");
        }

        public Computer MyPC
        {
            get{ return mypc; }
        }

        public void BuildHarddisk()
        {
            mypc.Harddisk = EHardDisk.CheapHarddisk;
        }

        public void BuildMonitor()
        {
            mypc.Monitor = EMonitor.CheapMonitor;
        }

        public void BuildProzessor()
        {
            mypc.Prozessor = EProzessor.CheapProzessor;
        }

        public void BuildRam()
        {
            mypc.Ram = ERAM.CheapRAM;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    public enum EMonitor
    {
        CheapMonitor,
        ExpensiveMonitor
    };
    public enum EProzessor
    {
        CheapProzessor,
        ExpensiveProzessor
    };
    public enum ERAM
    {
        CheapRAM,
        ExpensiveRAM
    };
    public enum EHardDisk
    {
        CheapHarddisk,
        ExpensiveHarddiks
    };
    class Components
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class PCManufacturer
    {
        public void Construct(IBuildPC computerBuilder)
        {
            computerBuilder.BuildMonitor();
            computerBuilder.BuildProzessor();
            computerBuilder.BuildHarddisk();
            computerBuilder.BuildRam();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class Computer
    {
       public  string pcName { get; }
        public EMonitor Monitor { get; set; }
        public EProzessor Prozessor { get; set; }
        public EHardDisk Harddisk { get; set; }
        public ERAM Ram { get; set; }

        public Computer (string name)
        {
            pcName = name;
        }
        public override string ToString()
        {
            return string.Format("Name: {0}\nMonitor: {1}\nProzessor: {2}\nHardDisk: {3}\nRam: {4} \n",pcName, Monitor,Prozessor,Harddisk,Ram);

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            PCManufacturer pc = new PCManufacturer();

            IBuildPC cheappc = new CheapPC();
            pc.Construct(cheappc);

            Console.WriteLine(cheappc.MyPC.ToString());

            IBuildPC expensivepc = new ExpensivePC();
            pc.Construct(expensivepc);
            Console.WriteLine(expensivepc.MyPC.ToString());
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class ExpensivePC : IBuildPC
    {
        Computer mypc;

        public ExpensivePC()
        {
            mypc = new Computer("Expensive PC");
        }

        public Computer MyPC
        {
            get { return mypc; }
        }

        public void BuildHarddisk()
        {
            mypc.Harddisk = EHardDisk.ExpensiveHarddiks;
        }

        public void BuildMonitor()
        {
            mypc.Monitor = EMonitor.ExpensiveMonitor;
        }

        public void BuildProzessor()
        {
            mypc.Prozessor = EProzessor.ExpensiveProzessor;
        }

        public void BuildRam()
        {
            mypc.Ram = ERAM.ExpensiveRAM;
        }
    }
}
